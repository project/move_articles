CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * UnInstallation
 * Maintainers


INTRODUCTION:
------------

This module used to move articles from one category to another category.


REQUIREMENTS
------------

No special requirements.


INSTALLATION:
-------------

1. Copy 'move_articles' folder to modules directory.
2. At admin/modules enable the 'Move Articles' module.


CONFIGURATION:
--------------

1. Activate module and go to admin/config/move-articles/config
2. Enter Node Taxonomy Machine Name
3. Enter Taxonomy Vocabulary Machine Name


UN-INSTALLATION:
----------------

To un-install the module,
1) Go to admin/modules
2) Find 'Move Articles'
3) Un-check the checkbox and save.
4) Then go to admin/modules/uninstall
5) Un-check the checkbox and click 'uninstall'.


MAINTAINERS:
------------

Vishal Goyal - https://www.drupal.org/u/vishalgoyal267
