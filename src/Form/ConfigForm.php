<?php

namespace Drupal\move_articles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'move_articles_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('move_articles.settings');
    $form = parent::buildForm($form, $form_state);
    $form['main_start'] = ['#markup' => '<div class="form-group-auto-repuslish"><h2>Config Settings</h2><p><a href="../move-articles/settings">Click here</a> to go migration category data page.</p><hr/>'];
    $form['main_end'] = ['#markup' => '</div>'];

    $form['category_field_name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Node Taxonomy Machine Name'),
      '#default_value' => $config->get('move_articles.category_field_name'),
      '#required' => TRUE,
    ];
    $form['taxonomy_vocab_name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Taxonomy Vocabulary Machine Name'),
      '#default_value' => $config->get('move_articles.taxonomy_vocab_name'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('move_articles.settings');
    $config->set('move_articles.category_field_name', $form_state->getValue('category_field_name'));
    $config->set('move_articles.taxonomy_vocab_name', $form_state->getValue('taxonomy_vocab_name'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}   */
  protected function getEditableConfigNames() {

    return [
      'move_articles.settings',
    ];
  }

}
