<?php

namespace Drupal\move_articles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configure example settings for this site.
 */
class MoveArticlesSettingForm extends ConfigFormBase {

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */

  protected $account;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(AccountInterface $account, Connection $connection, EntityTypeManagerInterface $entityTypeManager) {
    $this->account = $account;
    $this->database = $connection;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'move_articles_setting';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'move_articles.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get move_articles config value.
    $config = $this->config('move_articles.settings');
    // Get category field config value.
    $taxonomy_machine_name = $config->get('move_articles.category_field_name');
    // Get taxonomy vocab config value.
    $taxonomy_vocab_name = $config->get('move_articles.taxonomy_vocab_name');
    // Get user roles.
    $user = $this->account->getRoles();

    if (in_array('administrator', $user)) {
      $query = $this->database->select('taxonomy_term_field_data', 'ttfd');
      $query->fields('ttfd');
      $query->condition('ttfd.vid', $taxonomy_vocab_name);
      $result = $query->execute()->fetchAll();
      $catarray = [];
      foreach ($result as $row) {
        $catarray[$row->tid] = $row->name;
      }
      // Check taxonomy and vocab machine name empty or not.
      if ($taxonomy_machine_name == "" || $taxonomy_vocab_name == "") {
        $form['main_start'] = ['#markup' => '<div class="form-group-auto-repuslish"><h2>Migrate Content</h2><p>Please add <b>Node Taxonomy Machine Name</b> and <b>Taxonomy Vocabulary Machine Name</b> to access migration category data. <a href="../move-articles/config">Click here</a> to add.</p><hr/>'];
        $form['main_end'] = ['#markup' => '</div>'];
        return $form;
      }

      $form['main_start'] = ['#markup' => '<div class="form-group-auto-repuslish"><h2>Migrate Content</h2><p>Move Articles from one category to another category. <a href="../move-articles/config">Click here</a> to go config page.</p><hr/><h2>Config Details</h2><p>Node Taxonomy Machine Name = <b>' . $taxonomy_machine_name . '</b></p><p>Taxonomy Vocabulary Machine Name = <b>' . $taxonomy_vocab_name . '</b></p><hr/>'];
      $form['main_end'] = ['#markup' => '</div>'];
      $form["dropdown_start"] = ['#markup' => '<div class="dropdown_start">'];
      $form['catSelect'] = [
        '#type' => 'select',
        '#title' => $this->t('Select Source Category'),
        '#options' => $catarray,
        '#attributes' => ['class' => ['catSelect']],
        '#ajax' => [
          'callback' => [$this, 'showCatList'],
          'event' => 'change',
          'wrapper' => 'showcatdiv',
        ],
        "#required" => TRUE,
      ];

      $form['catSelect_another'] = [
        '#type' => 'select',
        '#title' => $this->t('Select Destination Category'),
        '#options' => $catarray,
        '#attributes' => ['class' => ['catSelect_another']],
        "#required" => TRUE,
      ];
      $form["dropdown_end"] = ['#markup' => '</div>'];
      $form["showcatdiv_start"] = ['#markup' => '<div id="showcatdiv"></div>'];
      if ($this->getFormId() == "move_articles_setting") {
        $form['actions']['submit']['#value'] = $this->t('Show Article List');
      }

      return parent::buildForm($form, $form_state);
    }
    else {
      $form['main_start'] = ['#markup' => '<div class="form-group-auto-repuslish"><p>Access Denied</p>'];
      $form['main_end'] = ['#markup' => '</div>'];
      return $form;
    }
  }

  /**
   * Ajax callback function showCatList.
   */
  public function showCatList(array $form, FormStateInterface $form_state) {
    // Get move_articles config value.
    $config = $this->config('move_articles.settings');
    // Get category field config value.
    $taxonomy_machine_name = $config->get('move_articles.category_field_name');

    $response = new AjaxResponse();
    $query = $this->database->select('taxonomy_term_field_data', 'ttfd');
    $query->join('node__' . $taxonomy_machine_name, 'nfc', 'ttfd.tid = nfc.' . $taxonomy_machine_name . '_target_id');
    $query->join('node_field_data', 'nfd', 'nfd.nid = nfc.entity_id');
    $query->fields('ttfd');
    $query->fields('nfc');
    $query->fields('nfd');
    $query->condition("ttfd.tid", $form_state->getValue("catSelect"));
    $result = $query->execute()->fetchAll();

    $ul = "<div class='form-group-auto-repuslish'><b>Node Lists</b></div><ul style='margin:0; padding:10px 0px 0px 15px;'>";
    if (count($result) != 0) {
      foreach ($result as $row) {
        $ul .= "<li>" . $row->title . "</li>";
      }
    }
    else {
      $ul .= "<li>No Data Found</li>";
    }
    $ul .= "</ul>";
    $response->addCommand(new HtmlCommand('#showcatdiv', $ul));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('catSelect') == "") {
      $form_state->setErrorByName('catSelect', $this->t('Please select source category.'));
      return FALSE;
    }
    if ($form_state->getValue('catSelect_another') == "") {
      $form_state->setErrorByName('catSelect_another', $this->t('Please select destination category.'));
      return FALSE;
    }
    if ($form_state->getValue('catSelect') == $form_state->getValue('catSelect_another')) {
      $form_state->setErrorByName('catSelect', $this->t('Category must be different.'));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get move_articles config value.
    $config = $this->config('move_articles.settings');
    // Get category field config value.
    $taxonomy_machine_name = $config->get('move_articles.category_field_name');

    $query = $this->database->select('taxonomy_term_field_data', 'ttfd');
    $query->join('node__' . $taxonomy_machine_name, 'nfc', 'ttfd.tid = nfc.' . $taxonomy_machine_name . '_target_id');
    $query->join('node_field_data', 'nfd', 'nfd.nid = nfc.entity_id');
    $query->fields('nfc', ['entity_id']);
    $query->condition("ttfd.tid", $form_state->getValue("catSelect"));
    $result = $query->execute()->fetchAll();
    foreach ($result as $row) {
      $query = $this->database->update('node__' . $taxonomy_machine_name);
      $query->fields([
        $taxonomy_machine_name . '_target_id' => $form_state->getValue('catSelect_another'),
      ]);
      $query->condition('entity_id', $row->entity_id);
      $query->condition($taxonomy_machine_name . '_target_id', $form_state->getValue('catSelect'));
      $query->execute();
    }

    $term_1 = $this->entityTypeManager->getStorage('taxonomy_term')->load($_POST['catSelect']);
    $name_1 = $term_1->getName();

    $term_2 = $this->entityTypeManager->getStorage('taxonomy_term')->load($_POST['catSelect_another']);
    $name_2 = $term_2->getName();

    $msg = "Articles moved from " . $name_1 . " to " . $name_2 . " category.";
    $this->messenger()->addStatus($msg);
  }

}
